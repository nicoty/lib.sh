# Description

Library files containing some commonly used functions in shell scripts.

[![Hits-of-Code](https://hitsofcode.com/gitlab/nicoty/lib.sh)](https://hitsofcode.com/view/gitlab/nicoty/lib.sh)

## License

This Work is distributed and dual-licensed under the terms of both the [MIT License](LICENSE-MIT) and the [Apache License 2.0](LICENSE-APACHE).

### Contribution

Unless You explicitly state otherwise, any Contribution submitted for inclusion in the Work by You, as defined in the Apache License 2.0, shall be dual-licensed as above, without any additional terms or conditions.
