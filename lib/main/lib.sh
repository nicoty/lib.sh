#!/bin/sh

main () { (
# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
  set -eu &&

# include libs
# directory that contains this script
  directory_self="$(dirname "$(readlink -f "${0}")")"
  file_self="$(basename "$(readlink -f "${0}")")"
  sh_lib="${directory_self}/../lib.sh"

# shellcheck source=../lib.sh
  . "${sh_lib}" &&

# set trap
  trap 'notify_exit_early ""' HUP INT QUIT ABRT ALRM TERM &&

# warn
  warning_privilege &&

# set variables
  version_self="0.1.0"
# will_exit used to allow other options to be parsed too
  will_exit=1

# define internal functions
  print_help () {
    printf 'Some commonly used functions in shell scripts.

USAGE:
  To be able to use the functions this library
  script provides, insert the following line into
  your script:

  . %s

FUNCTIONS:
  print_version <FILENAME> <VERSION>
    Print FILENAME and VERSION. Used when
    invoking executable scripts with the
    `--version` flag.

  print_coloured <STRING> <COLOUR>
    Print STRING in the colour COLOUR (case-
    insensitive). Valid COLOUR values are:
      30, black
        Print STRING in the colour black.
      31, r, red
        Print STRING in the colour red.
      32, g, green
        Print STRING in the colour green.
      33, y, yellow
        Print STRING in the colour yellow.
      34, b, blue
        Print STRING in the colour blue.
      35, m, magenta
        Print STRING in the colour magenta.
      36, c, cyan
        Print STRING in the colour cyan.
      37, w, white
        Print STRING in the colour white.

  error <STRING>
    Print "ERROR: STRING" in the colour red.

  error_arguments_contained
    Used with `getopts` built-in shell command.
    Print "Option "--OPTION" should not
    contain any arguments." in red, where
    OPTION is the option passed to `getopts`.

  error_option_invalid
    Used with `getopts` built-in shell command.
    Print "Option "--OPTION" is invalid."
    in red, where OPTION is the option passed
    to `getopts`.

  notify_exit_early [DIRECTORIES/FILES]
    Used with `trap` built-in shell command.
    Print "Exited too early." in red, then remove
    DIRECTORIES/FILES. If no directories or files
    are to be removed, you should pass an empty
    string "" to `notify_exit_early` instead.

  warning <STRING>
    Print "WARNING: STRING" in the colour yellow.

  warning_privilege
    Warn if the shell script is being run as "root"
    or as a user in the "wheel" group (GIDs 0 and
    4, respectively), then present a prompt to
    continue script execution. Accepts "y" or "yes"
    (case-insensitive) as true and any other input
    as false.
' "$(readlink --canonicalize "${directory_self}/../${file_self}")" >&2
  }

# argument parsing loop from https://stackoverflow.com/a/28466267
  while getopts hVx-: argument; do
    case "${argument}" in
      h )
        print_help >&2 &&
        will_exit=0
      ;;
      V )
        print_version "${file_self}" "${version_self}" >&2 &&
        will_exit=0
      ;;
      x )
        set -x
      ;;
      - )
        case "${OPTARG}" in
          help )
            print_help >&2 &&
            will_exit=0
          ;;
          version )
            print_version "${file_self}" "${version_self}" >&2 &&
            will_exit=0
          ;;
          debug )
            set -x
          ;;
          help* | version* | debug* )
            error_arguments_contained >&2 &&
            exit 2
          ;;
#         "--" terminates argument processing
          '' )
            break
          ;;
          * )
            error_option_invalid >&2 &&
            exit 2
          ;;
        esac
      ;;
#     getopts already reported the illegal option
      \? )
        exit 2
      ;;
    esac
  done &&
# remove parsed options and arguments from "${@}" list
  shift $((OPTIND-1)) &&

  if test "${will_exit}" -eq 0 ; then
    exit 0
  fi
) }

main "${@}"
